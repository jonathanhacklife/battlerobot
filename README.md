# BattleRobot
## Descripción
- [ ] Lucas, si podés adjuntale una descripción del proyecto

Haga saber a la gente lo que su proyecto puede hacer específicamente. Proporcione el contexto y añada un enlace a cualquier referencia con la que los visitantes no estén familiarizados. También se puede añadir aquí una lista de características o una subsección de antecedentes. Si hay alternativas a su proyecto, este es un buen lugar para enumerar los factores diferenciadores.

## Diseño
- [ ] Documentación del diseño de Vir

Dependiendo de lo que estés haciendo, puede ser una buena idea incluir capturas de pantalla o incluso un vídeo (con frecuencia verás GIFs en lugar de vídeos reales). Herramientas como ttygif pueden ayudar, pero echa un vistazo a Asciinema para un método más sofisticado.

## Instalación
- [ ] Jona adjunta instalación del entorno

Dentro de un ecosistema particular, puede haber una forma común de instalar cosas, como el uso de Yarn, NuGet, o Homebrew. Sin embargo, considere la posibilidad de que quien esté leyendo su README sea un novato y quiera más orientación. Enumerar los pasos específicos ayuda a eliminar la ambigüedad y hace que la gente utilice su proyecto lo más rápidamente posible. Si sólo se ejecuta en un contexto específico, como una versión de lenguaje de programación o un sistema operativo concreto, o tiene dependencias que deben instalarse manualmente, añade también una subsección de Requisitos.

## Uso
- [ ] Ger puede ir comentando el uso de ambas plataformas

Para testear la conexión a WIFI, ejecute el archivo robocode.ino
Para testear la conexión a WIFI con servidor interno en el ESP8266, ejecute el archivo wifiLed.ino

Utiliza los ejemplos libremente, y muestra la salida esperada si puedes. Es útil tener en línea el ejemplo de uso más pequeño que pueda demostrar, mientras que proporciona enlaces a ejemplos más sofisticados si son demasiado largos para incluirlos razonablemente en el README.

## Soporte
Indique a la gente dónde puede acudir para obtener ayuda. Puede ser cualquier combinación de un rastreador de problemas, una sala de chat, una dirección de correo electrónico, etc.

## Hoja de ruta
Si tienes ideas para futuras versiones, es una buena idea incluirlas en el LÉAME.

## Contribución
Indique si está abierto a contribuciones y cuáles son sus requisitos para aceptarlas.

Para la gente que quiere hacer cambios en su proyecto, es útil tener alguna documentación sobre cómo empezar. Quizás haya un script que deban ejecutar o algunas variables de entorno que deban configurar. Haz que estos pasos sean explícitos. Estas instrucciones también podrían ser útiles para tu futuro yo.

También puedes documentar los comandos para desentrañar el código o ejecutar pruebas. Estos pasos ayudan a asegurar la alta calidad del código y a reducir la probabilidad de que los cambios rompan algo inadvertidamente. Tener instrucciones para ejecutar las pruebas es especialmente útil si requiere una configuración externa, como iniciar un servidor Selenium para probar en un navegador.

## Autores y agradecimientos
Muestra tu agradecimiento a los que han contribuido al proyecto.

## Licencia
Para proyectos de código abierto, diga cómo está licenciado.

## Estado del proyecto
- [ ] Documentación del proyecto en el Readme
- [ ] Levantar servidor global para ver estadísticas
- [ ] Pruebas de velocidad
- [ ] Pruebas de seguridad
- [ ] Refactorización de código

Si te has quedado sin energía o tiempo para tu proyecto, pon una nota en la parte superior del LÉEME diciendo que el desarrollo se ha ralentizado o se ha detenido por completo. Alguien puede elegir bifurcar su proyecto o ofrecerse como mantenedor o propietario, permitiendo que su proyecto siga adelante. También puedes hacer una petición explícita de mantenedores.