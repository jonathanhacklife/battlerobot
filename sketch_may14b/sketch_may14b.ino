void setup() {
  Serial.begin(115200);
  // Inicializar las variables de salida como salidas
//MOTOR 1
  pinMode(5, OUTPUT);
  pinMode(4, OUTPUT);
//MOTOR 2
  pinMode(0, OUTPUT);
  pinMode(2, OUTPUT);
  Serial.print("Encendido");
}

void loop() {
  digitalWrite(5, HIGH);
  Serial.println("ADELANTE ON");
  delay(1000);
  digitalWrite(5, LOW);
  Serial.println("ADELANTE OFF");
  delay(1000);
  digitalWrite(4, HIGH);
  Serial.println("ATRAS ON");
  delay(1000);
  digitalWrite(4, LOW);
  Serial.println("ATRAS OFF");
  delay(1000);

// MOTOR 2
  digitalWrite(0, HIGH);
  Serial.println("ADELANTE ON");
  delay(1000);
  digitalWrite(0, LOW);
  Serial.println("ADELANTE OFF");
  delay(1000);
  digitalWrite(2, HIGH);
  Serial.println("ATRAS ON");
  delay(1000);
  digitalWrite(2, LOW);
  Serial.println("ATRAS OFF");
  delay(1000);

}
