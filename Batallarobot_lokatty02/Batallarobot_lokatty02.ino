#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif


// Asignar variables de salida a los pines GPIO
const int motor1ad = 5; //
const int motor1at = 4; //
const int motor2ad = 0; //
const int motor2at = 2; //
const int Vizq = 19 ; //velocidad de motores izquierdos
const int Vder = 18 ; //velocidad de motores derechos
int estado = -1; //revisar aca

BluetoothSerial SerialBT;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("UNNOMBREMASDOS"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");

  pinMode(motor1ad, OUTPUT);
  pinMode(motor1at, OUTPUT);
  pinMode(motor2ad, OUTPUT);
  pinMode(motor2at, OUTPUT);
  pinMode(Vizq, OUTPUT);
  pinMode(Vder, OUTPUT);
}

void loop() {
  if (SerialBT.available()) {
    estado = SerialBT.read();

    //estado 1 es Adelante
    if (estado == '1') {
      digitalWrite(motor2ad, HIGH);
      digitalWrite(motor1ad, HIGH);
      analogWrite(Vizq, 255);
      analogWrite(Vder, 255);
    }
    //estado 2 es atras
    if (estado == '2') {
      digitalWrite(motor2at, HIGH);
      digitalWrite(motor1at, HIGH);
      analogWrite(Vizq, 255);
      analogWrite(Vder, 255);
    }
    // estado 3 es el giro a la izquierda
    if (estado == '3') {
      analogWrite(Vizq, 80);
    }

    // estado 4 es el giro a la derecha
    if (estado == '4') {
      analogWrite(Vder, 80);
    }


    // aca es cuando suelto el boton

    // estado 5 es soltar Adelante
    if (estado == '5') {
      digitalWrite(motor1ad, LOW);
      digitalWrite(motor2ad, LOW);
    }

    // estado 6 soltar atras
    if (estado == '6') {
      digitalWrite(motor1at, LOW);
      digitalWrite(motor2at, LOW);
    }

    //estado 7 es soltar giro a izquierda entonces vuelve a velocidad normal el motor
    if (estado == '7') {
      analogWrite(Vizq, 255);
    }

    //estado 8 es soltar giro a derecha entonces vuelve a velocidad normal el motor
    if (estado == '8') {
      analogWrite(Vder, 255);
    }

  }
}
