#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif


// Asignar variables de salida a los pines GPIO
const int motor1ad = 5; // boton 2 y 6
const int motor1at = 4; // boton 4 y 8
const int motor2ad = 0; // boton 1 y 5
const int motor2at = 2; // boton 3 y 7
int estado = 1; //revisar aca

BluetoothSerial SerialBT;


void setup() {
  pinMode(motor1ad, OUTPUT);
  pinMode(motor1at, OUTPUT);
  pinMode(motor2ad, OUTPUT);
  pinMode(motor2at, OUTPUT);

  Serial.begin(115200);
  SerialBT.begin("HACKBOT"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");

}


void loop() {
  if (SerialBT.available()) {
    estado = SerialBT.read();
  }

  if (estado == '1') {
    digitalWrite(motor2ad, HIGH);
  }
  if (estado == '2') {
    digitalWrite(motor1ad, HIGH);
  }

  if (estado == '3') {
    digitalWrite(motor2at, HIGH);
  }
  if (estado == '4') {
    digitalWrite(motor1at, HIGH);
  }

  // aca es cuando suelto el boton 
  if (estado == '5') {
    digitalWrite(motor2ad, LOW);
  }
  if (estado == '6') {
    digitalWrite(motor1ad, LOW);
  }
  if (estado == '7') {
    digitalWrite(motor2at, LOW);
  }
  if (estado == '8') {
    digitalWrite(motor1at, LOW);
  }

}
