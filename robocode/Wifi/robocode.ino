#include <Arduino.h>
#include <ESP8266WiFi.h>
// DOCUMENTACIÓN: https://roboticadiy.com/esp8266-make-your-own-led-control-web-server-in-arduino-ide/
// Introduce el nombre de tu red wifi y la contraseña Wifi
const char* ssid = "ECAlmagro";
const char* password = "a3delateneo";

// Establezca el número de puerto del servidor web en 80
WiFiServer server(80);

// Variable para almacenar la petición HTTP
String header = "";

// Estas variables almacenan el estado actual de la salida del LED
bool adelanteState = false;
bool atrasState = false;
bool izquierdaState = false;
bool derechaState = false;

// Asignar variables de salida a los pines GPIO
const int motor1ad = 5;
const int motor1at = 4;
const int motor2ad = 0;
const int motor2at = 2;

// Hora actual
unsigned long currentTime = millis();
// Tiempo anterior
unsigned long previousTime = 0;
// Definir el tiempo de espera en milisegundos (ejemplo: 2000ms = 2s)
const long timeoutTime = 2000;

void adelante(bool estado) {
  if (estado) {
    //Dir Motor 1
    digitalWrite(motor1ad, HIGH);
    digitalWrite(motor1at, LOW);
    // Dir Motor 2
    digitalWrite(motor2ad, HIGH);
    digitalWrite(motor2at, LOW);
  }
  else {
    //Dir Motor 1
    digitalWrite(motor2ad, LOW);
    digitalWrite(motor2at, LOW);
    // Dir Motor 2
    digitalWrite(motor1ad, LOW);
    digitalWrite(motor1at, LOW);
  }
}

void atras(bool estado) {
  if (estado) {
    //Dir Motor 1
    digitalWrite(motor2ad, LOW);
    digitalWrite(motor2at, HIGH);
    //Dir Motor 2
    digitalWrite(motor1ad, LOW);
    digitalWrite(motor1at, HIGH);
  }
  else {
    //Dir Motor 1
    digitalWrite(motor2ad, LOW);
    digitalWrite(motor2at, LOW);
    //Dir Motor 2
    digitalWrite(motor1ad, LOW);
    digitalWrite(motor1at, LOW);
  }
}

void derecha(bool estado) {
  if (estado) {
    //Dir Motor 1
    digitalWrite(motor2ad, HIGH);
    digitalWrite(motor2at, LOW);
    //Dir Motor 2
    digitalWrite(motor1ad, LOW);
    digitalWrite(motor1at, LOW);
  }
  else {
    //Dir Motor 1
    digitalWrite(motor2ad, LOW);
    digitalWrite(motor2at, LOW);
    //Dir Motor 2
    digitalWrite(motor1ad, LOW);
    digitalWrite(motor1at, LOW);
  }
}

void izquierda(bool estado) {
  if (estado) {
    //Dir Motor 1
    digitalWrite(motor2ad, LOW);
    digitalWrite(motor2at, LOW);
    //Dir Motor 2
    digitalWrite(motor1ad, HIGH);
    digitalWrite(motor1at, LOW);
  }
  else {
    //Dir Motor 1
    digitalWrite(motor2ad, LOW);
    digitalWrite(motor2at, LOW);
    //Dir Motor 2
    digitalWrite(motor1ad, LOW);
    digitalWrite(motor1at, LOW);
  }
}

void setup() {
  Serial.begin(115200);
  // Inicializar las variables de salida como salidas
  pinMode(motor1ad, OUTPUT);
  pinMode(motor1at, OUTPUT);
  pinMode(motor2ad, OUTPUT);
  pinMode(motor2at, OUTPUT);

  // Conéctate a la red Wi-Fi con el SSID y la contraseña
  Serial.print("Conectar con ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Imprimir la dirección IP local e iniciar el servidor web
  Serial.println("");
  Serial.println("WiFi conectado.");
  Serial.println("IP: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop() {
  WiFiClient client = server.available(); // Escuchar a los clientes que llegan

  if (client) { // Si se conecta un nuevo cliente,
    Serial.println("Conectado."); // imprimir un mensaje en el puerto serie
    String currentLine = ""; // hacer un String para contener los datos entrantes del cliente
    currentTime = millis();
    previousTime = currentTime;
    while (client.connected() && currentTime - previousTime <= timeoutTime) { // bucle mientras el cliente está conectado
      currentTime = millis();
      if (client.available()) { // si hay bytes que leer del cliente,
        char c = client.read(); // leer un byte, entonces
        Serial.write(c); // imprimirlo en el monitor de serie
        header += c;
        if (c == '\n') { // si el byte es un carácter de nueva línea
          // si la línea actual está en blanco, tienes dos caracteres de nueva línea seguidos.
          // es el fin de la petición HTTP del cliente, así que envía una respuesta:
          if (currentLine.length() == 0) {
            // Las cabeceras HTTP siempre comienzan con un código de respuesta (por ejemplo, HTTP/1.1 200 OK)
            // y un tipo de contenido para que el cliente sepa lo que viene, y luego una línea en blanco:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();

            // enciende y apaga los GPIOs
            if (header.indexOf("GET /adelante/on") >= 0) {
              Serial.println("adelante ON");
              adelanteState = true;
              adelante(adelanteState);
            }
            else if (header.indexOf("GET /adelante/off") >= 0) {
              Serial.println("adelante OFF");
              adelanteState = false;
              adelante(adelanteState);
            }
            else if (header.indexOf("GET /atras/on") >= 0) {
              Serial.println("ATRAS ON");
              atrasState = true;
              atras(atrasState);
            }
            else if (header.indexOf("GET /atras/off") >= 0) {
              Serial.println("ATRAS OFF");
              atrasState = false;
              atras(atrasState);
            }
            else if (header.indexOf("GET /izquierda/on") >= 0) {
              Serial.println("IZQUIERDA ON");
              izquierdaState = true;
              izquierda(izquierdaState);
            }
            else if (header.indexOf("GET /izquierda/off") >= 0) {
              Serial.println("IZQUIERDA OFF");
              izquierdaState = false;
              izquierda(izquierdaState);
            }
            else if (header.indexOf("GET /derecha/on") >= 0) {
              Serial.println("DERECHA ON");
              derechaState = true;
              derecha(derechaState);
            }
            else if (header.indexOf("GET /derecha/off") >= 0) {
              Serial.println("DERECHA OFF");
              derechaState = false;
              derecha(derechaState);
            }

            // Mostrar la página web HTML
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS para estilizar
            client.println("<style>html,body {background-color: black;color: white;font-family: 'Roboto', sans-serif;margin: 0px auto;text-align: center;height: 100%;} body {display: flex;flex-direction: column;justify-content: space-between;} p {margin: 0px;} .controls {display: flex;justify-content: space-between;align-items: center;} .controls>div {background-color: transparent;text-align: center;border-radius: 10px;} .movimiento {display: flex;flex-direction: column;justify-content: space-between;align-items: center;width: 40%;} .direccion {display: flex;justify-content: space-between;width: 100%; align-items: center;} button {background-color: white;color: black;padding: 20px;border-radius: 20px;text-transform: uppercase;font-size: 2em;cursor: pointer;width: 100px;height: 100px;background-size: cover;} button:active {background-color: lightslategrey;}</style></head>");

            // Título de la página web
            client.println("<body><h1>Batalla de R0b0ts</h1>");

            // Indicación del estado actual y botones ON/OFF para GPIO 2 adelante
            client.println("<div class='controls'><div class='movimiento'><div class='adelante'>adelante " + adelanteState);
            // Si la salidaRedState está apagada, muestra el botón OFF
            if (adelanteState == false) {
              client.println("<a href=\"/adelante/on\"><button></button></a></div>");
            }
            else {
              client.println("<a href=\"/adelante/off\"><button></button></a></div>");
            }

            // Muestra el estado actual, y botones ON/OFF para GPIO 4 IZQUIERDA
            client.println("<div class='direccion'><div class='izquierda'>Izquierda " + izquierdaState);
            // Si el atrasState está apagado, muestra el botón OFF
            if (izquierdaState == false) {
              client.println("<a href=\"/izquierda/on\"><button></button></a></div>");
            }
            else {
              client.println("<a href=\"/izquierda/off\"><button></button></a></div>");
            }

            // Indicación del estado actual y botones ON/OFF para GPIO 5 ATRAS
            client.println("<div class='derecha'>Derecha " + derechaState);
            // Si la salidaBlueState está apagada, muestra el botón OFF
            if (derechaState == false) {
              client.println("<a href=\"/derecha/on\"><button></button></a></div>");
            }
            else {
              client.println("<a href=\"/derecha/off\"><button></button></a></div>");
            }

            // Muestra el estado actual, y botones ON/OFF para GPIO 5 ATRAS
            client.println("<div class='atras'>Atras " + atrasState);
            // Si el atrasState está apagado, muestra el botón OFF
            if (atrasState == false) {
              client.println("<a href=\"/atras/on\"><button></button></a></div></div>");
            }
            else {
              client.println("<a href=\"/atras/off\"><button></button></a></div></div>");
            }

            // Botones de acción para los GPIOs
            client.println("<div class='acciones'><button>A</button><button>B</button><button>C</button><button>D</button></div></div>");
            client.println("</body></html>");

            // La respuesta HTTP termina con otra línea en blanco
            client.println();
            // Salir del bucle while
            break;
          }
          else { // si tiene una nueva línea, entonces borre currentLine
            currentLine = "";
          }
        }
        else if (c != '\r') { // si tiene algo más que un carácter de retorno de carro,
          currentLine += c; // añadirlo al final de la línea actual
        }
      }
    }
    // Borrar la variable de cabecera
    header = "";
    // Cerrar la conexión
    client.stop();
    Serial.println("Desconectado");
    Serial.println("");
  }
}
